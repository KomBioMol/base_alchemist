import basemods
from optparse import OptionParser
import os, sys


class Top:
    def __init__(self, filename_top, filename_struct, lesion, res, noalch, base=None):
        self.home_script = sys.path[0] + '/'
        self.noalch = noalch
        self.res = res  # list of residues to modify
        self.fname = filename_top
        self.contents = open(self.fname).readlines() if not base else base.contents
        self.sections = {'h': 'header', 'm': 'moleculetype', 't': 'atoms', 'b': 'bonds', 'p': 'pairs',
                         'a': 'angles', 'd': 'dihedrals', 'i': 'dihedrals', 'f': 'footer'}
        self.section_npar = {'b': 2, 'a': 3, 'p': 2, 'd': 4, 'i': 4, 't': 7}
        self.dict = {section: self.get_section(section) for section in self.sections.keys()}
        self.ter = self.dict['t'][-1].split()[2:5]  # needed for insertion of dummies by Pdb()
        self.pdb = Pdb(filename_struct, self.res, self.ter, self)
        avail_lesions = {'8oxog': basemods.OxoG, 'fapyg': basemods.FapyG,
                         '6ma': basemods.mA, '5mc': basemods.mC, 'ttd': basemods.TTd1,
                         'ttdd': basemods.TTd2, 'ap': self.choose_ap()}
        self.pdb.lesion = self.lesion = avail_lesions[lesion.lower()](self.pdb)
        self.check_base()
        self.params = {'b': 1, 'p': 1, 'a': 1, 'd': 9, 'i': 4}

    def process_top(self):
        if self.noalch:
            self.add_atoms()
            self.remove_atoms()
        else:
            self.add_state_b()
            self.add_dummies()
            self.modify_types()
        for ptype in 'bpadi':
            self.add_param(ptype)
        for ptype in 'badi':
            self.modify_param(ptype)
        if self.noalch:
            self.mod_state_a()
            self.rename_atoms()
        new_content = []
        for i in 'hmtbpadif':
            for line in self.dict[i]:
                new_content.append(line)
            new_content.append('\n')
        self.contents = new_content

    def add_state_b(self):
        atoms = self.dict['t']
        chgs = self.lesion.get_charges()
        fstring = '{:>7s}{:>11.4f}{:>11.3f}'
        for res in self.res:
            for l in range(len(atoms)):
                line = atoms[l]
                if len(line.split()) > 7 and line.split()[3] == self.lesion.initbase and int(line.split()[2]) == res \
                        and not line.startswith(';'):
                    name = line.split()[4]
                    atype = line.split()[1]
                    mass = float(line.split()[7])
                    charge = float(chgs[name])
                    split = line.find(';') - 1 if ';' in line else len(line) - 1
                    state_a_end = len(line[:split].rstrip())
                    atoms[l] = line[:state_a_end] + fstring.format(atype, charge, mass) + line[state_a_end:]
        self.dict['t'] = atoms
        
    def mod_state_a(self):
        """
        When non-alchemical changes are introduced,
        this fn changes all parameters within state A
        (there is no state B in this case)
        :return: None
        """
        atoms = self.dict['t']
        chgs = self.lesion.get_charges()
        to_mod = self.lesion.get_alttypes()
        try:
            name_aliases = self.lesion.aliases
        except AttributeError:
            name_aliases = {}
        fstring = "{:>6s}{:>11s}{:>7s}{:>7s}{:>7s}{:>7s}{:>11.4f}{:>11.3f}\n"
        for res in self.res:
            for l in range(len(atoms)):
                line = atoms[l]
                if len(line.split()) > 7 and line.split()[3] == self.lesion.initbase and int(line.split()[2]) == res \
                        and not line.startswith(';'):
                    lspl = line.split()
                    name = lspl[4]
                    try:
                        charge = float(chgs[name])
                    except KeyError:
                        charge = self.lesion.dummies[name][1][1]
                    atype = lspl[1]
                    mass = float(lspl[7])
                    if name in to_mod.keys():
                        atype = to_mod[name][0]
                        if to_mod[name][1] is not None:
                            mass = to_mod[name][1]
                    if name in name_aliases.keys():
                        name = name_aliases[name]  # todo maybe move to rename?
                    atoms[l] = fstring.format(lspl[0], atype, lspl[2], self.lesion.resname, name, lspl[5], charge, mass)
        self.dict['t'] = atoms

    def modify_types(self):
        atoms = self.dict['t']
        to_mod = self.lesion.get_alttypes()
        try:
            name_aliases = self.lesion.aliases
        except AttributeError:
            name_aliases = {}
        fstring = "{:>6d}{:>11s}{:>7d}{:>7s}{:>7s}{:>7d}{:>11.4f}{:>11.3f}{:>7s}{:>11.4f}{:>11.3f}"
        for res in self.res:
            for l in range(len(atoms)):
                line = atoms[l]
                if (len(line.split()) > 7 and line.split()[3] == self.lesion.initbase
                        and int(line.split()[2]) == res
                        and not line.startswith(';')):
                    name = line.split()[4]
                    split = line.find(';') - 1 if ';' in line else len(line) - 1
                    state_end = len(line[:split].rstrip())
                    splitline = line.split()
                    if name in name_aliases.keys():
                        splitline[4] = name_aliases[name]
                    for i in [0, 2, 5]:
                        splitline[i] = int(splitline[i])
                    for i in [6, 7, 9, 10]:
                        splitline[i] = float(splitline[i])
                    if line.split()[4] in to_mod.keys():
                        splitline[8] = to_mod[name][0]
                        if to_mod[name][1] is not None:  # only mod type
                            splitline[10] = to_mod[name][1]
                    atoms[l] = fstring.format(*splitline) + line[state_end:]
        self.dict['t'] = atoms

    def add_dummies(self):
        atoms = self.dict['t']
        fstring = "{:>6d}{:>11s}{:>7d}{:>7s}{:>7s}{:>7d}{:>11.4f}{:>11.3f}{:>7s}{:>11.4f}{:>11.3f}\n"
        if self.lesion.get_dummies():
            for res in self.res:
                end = self.locate_residue_end(res)
                self.offset_numbering(len(self.lesion.get_dummies()), end+1)
                for d in self.lesion.get_dummies():
                    num = [n for n, l in enumerate(atoms) if l.split()[0] == str(end)][0]
                    atoms.insert(num+1, fstring.format(end+1, d[0], res, self.lesion.initbase, d[1], end+1, *d[2:]))
                    end += 1
            self.dict['t'] = atoms
    
    def add_atoms(self):
        """
        Same as add_dummies, except for introduces
        single-state "regular" atoms instead of alchemical
        atoms that get morphed into dummies (hence the
        different formatting string)
        :return: None
        """
        atoms = self.dict['t']
        fstring = "{:>6d}{:>11s}{:>7d}{:>7s}{:>7s}{:>7d}{:>11.4f}{:>11.3f}\n"
        if self.lesion.get_dummies():
            for res in self.res:
                end = self.locate_residue_end(res)
                self.offset_numbering(len(self.lesion.get_dummies()), end+1)
                for d in self.lesion.get_dummies():
                    num = [n for n, l in enumerate(atoms) if l.split()[0] == str(end)][0]
                    atoms.insert(num+1, fstring.format(end+1, d[4], res, self.lesion.initbase, d[1], end+1, *d[2:]))
                    end += 1
            self.dict['t'] = atoms
    
    def rename_atoms(self):
        to_mod = self.lesion.get_alttypes()
        for res in self.res:
            for d in to_mod.keys():
                if len(to_mod[d]) == 3:
                    num, _ = self.find_line('t', [d], res)
                    line = self.dict['t'][num]
                    self.dict['t'][num] = "{:31s}{:>7s}{:s}".format(line[:31], to_mod[d][2], line[38:])
                    # TODO alt
            
    def remove_atoms(self):
        remove = [x for x in self.lesion.alttypes.keys() if self.lesion.alttypes[x][0].startswith("DUM")]
        for rem in remove:
            for res in self.res:
                lnum, at_num = self.find_line('t', [rem], res)
                self.remove_params(at_num)
                self.offset_numbering(-1, at_num+1)
                self.dict['t'].pop(lnum)  # add remove_params
                
    def remove_params(self, index):
        for section in 'bpadi':
            section_content = self.dict[section]
            npar = self.section_npar[section]
            new_content = []
            for linenum, line in enumerate(section_content):
                lspl = line.split()
                if len(lspl) > npar and not lspl[0].startswith('[') and not lspl[0].startswith(';'):
                    if str(index) not in lspl[:npar]:
                        new_content.append(section_content[linenum])
            self.dict[section] = section_content
    
    def locate_residue_end(self, residue):
        atoms = self.dict['t']
        res_lines = [line for line in atoms if len(line.split()) > 3 and line.split()[2] == str(residue)
                     and not line.strip()[0] in [';', '[']]
        if len(set([line.split()[3] for line in res_lines])) > 1:
            raise RuntimeError("found more than one residue name with ID\n{}\n"
                               "Consider renumbering DNA residues in your PDB file to avoid duplicates".format(residue))
        last_res_line = res_lines[-1]
        return int(last_res_line.split()[0])

    def offset_numbering(self, offset, startfrom=0):
        offset = int(offset)
        startfrom = int(startfrom)
        self.offset_atoms(offset, startfrom)
        self.offset_params(offset, startfrom)

    def offset_atoms(self, offset, startfrom):
        section_content = self.dict['t']
        for linenum, line in enumerate(section_content):
            lspl = line.split()
            if len(lspl) > 7 and not lspl[0].startswith(';') and int(lspl[0]) >= startfrom:
                new_line = ('{:6d}{:>11s}{:>7s}{:>7s}{:>7s}{:>7d} ' +
                            (len(lspl) - 6) * '{:>10s} ').format(int(lspl[0]) + offset, *lspl[1:5],
                                                                 int(lspl[5]) + offset, *lspl[6:]) + '\n'
                section_content[linenum] = new_line
        self.dict['t'] = section_content

    def offset_params(self, offset, startfrom):
        for section in 'bpadi':
            section_content = self.dict[section]
            npar = self.section_npar[section]
            for linenum, line in enumerate(section_content):
                lspl = line.split()
                if len(lspl) > npar and not lspl[0].startswith('[') and not lspl[0].startswith(';'):
                    new_line = (npar * '{:>5d} ' + '{:>5s} ' +
                                (len(lspl) - npar - 1) * '{:>10s} ').format(*[int(x) + (offset * (int(x) >= startfrom))
                                                                              for x in lspl[:npar]], *lspl[npar:])+'\n'
                    section_content[linenum] = new_line
            self.dict[section] = section_content

    def modify_param(self, ptype):
        section = self.dict[ptype]
        mods = self.lesion.get_mod_param(ptype)
        for res in self.res:
            for m in mods.keys():
                line = self.find_line(ptype, m, res)
                if line is not None:
                    if ptype in 'di' and len(mods[m]) > 6:  # separate handling of multiple dihedrals
                        assert len(mods[m]) % 6 == 0
                        base = section.pop(line).rstrip()
                        for q in range(int(len(mods[m])/6)):
                            if self.noalch:
                                text = base + "".join("{:>10}".format(a) for a in mods[m][6*q:6*(q+1)][3:]) + '\n'
                            else:
                                text = base + "".join("{:>10}".format(a) for a in mods[m][6*q:6*(q+1)]) + '\n'
                            section.insert(line+q, text)
                    else:
                        if self.noalch:
                            nprm = len(mods[m]) // 2
                            section[line] = section[line].rstrip() + \
                                            "".join("{:>10}".format(a) for a in mods[m][nprm:]) + '\n'
                        else:
                            section[line] = section[line].rstrip() + "".join("{:>10}".format(a) for a in mods[m]) + '\n'
                else:
                    print(m, mods[m])
        self.dict[ptype] = section

    def find_line(self, ptype, atoms, res):
        names, nums = self.get_nums(res)
        q = len(atoms)
        section = self.dict[ptype]
        for l in range(len(section)):
            if ptype != 't':
                if not section[l][0] in "[;" and all(int(section[l].split()[a]) in names.keys() for a in range(q)):
                    if (all([names[int(section[l].split()[a])] == atoms[a] for a in range(q)])
                            or all([names[int(section[l].split()[a])] == atoms[q - a - 1] for a in range(q)])):
                        return l
            else:
                if not section[l].strip()[0] in "[;" and len(section[l].split()) >= 5:
                    if section[l].split()[4] == str(atoms[0]) and section[l].split()[2] == str(res):
                        return l, int(section[l].split()[0])
        if ptype != 't':
            print('atoms {} not found'.format(tuple(nums[a] for a in atoms)))
        else:
            raise RuntimeError('atom {} not found in residue {}'.format(atoms[0], res))

    def add_param(self, ptype):
        section = self.dict[ptype]
        news = self.lesion.get_new_param(ptype)
        for res in self.res:
            names, nums = self.get_nums(res)
            if self.lesion.name == 'TTd2':
                nam, num = self.get_nums(res-1)
                names.update(nam)
                nums.update(num)
            for n in news.keys():
                if ptype in 'di' and len(news[n]) > 6:  # separate handling of multiple dihs
                    assert len(news[n]) % 6 == 0
                    for q in range(int(len(news[n]) / 6)):
                        if self.noalch:
                            text = ' '.join('{:5d}'.format(nums[a]) for a in n) + '{:6d}'.format(self.params[ptype]) \
                                   + "".join("{:>10}".format(a) for a in news[n][6 * q:6 * (q + 1)][3:]) + '\n'
                        else:
                            text = ' '.join('{:5d}'.format(nums[a]) for a in n) + '{:6d}'.format(self.params[ptype]) \
                                    + "".join("{:>10}".format(a) for a in news[n][6*q:6*(q+1)]) + '\n'
                        section.append(text)
                else:
                    try:
                        if self.noalch:
                            nprm = len(news[n]) // 2
                            text = ' '.join('{:5d}'.format(nums[a]) for a in n) + '{:6d}'.format(self.params[ptype])\
                                    + "".join("{:>10}".format(a) for a in news[n][nprm:]) + '\n'
                        else:
                            text = ' '.join('{:5d}'.format(nums[a]) for a in n) + '{:6d}'.format(self.params[ptype]) \
                                   + "".join("{:>10}".format(a) for a in news[n]) + '\n'
                    except KeyError:
                        print(names)
                        raise RuntimeError("At least one of the atoms {} was not found in residue {}; please make sure "
                                           "your molecule is complete".format(n, res))
                    section.append(text)
        self.dict[ptype] = section

    def get_bos(self, ptype):  # finds line num that starts respective section
        lines = [i for i in range(len(self.contents)) if len(self.contents[i].split()) > 2
                 and self.contents[i].split()[1] == self.sections[ptype]]
        if len(lines) == 0:
            raise RuntimeError("Section {} not found in the input topology file".format(self.sections[ptype]))
        if ptype in 'mtbpad':
            return lines[0]
        elif ptype == 'i':
            return lines[1]

    def get_section(self, ptype):
        order = {'m': 't', 't': 'b', 'b': 'p', 'p': 'a', 'a': 'd', 'd': 'i'}
        if ptype in 'mtbpad':
            return [l for l in self.contents[self.get_bos(ptype):self.get_bos(order[ptype])] if not l.isspace()]
        elif ptype == 'i':
            return [line for line in self.contents[self.get_bos(ptype):] if line.startswith('[ dih')
                    or line.startswith(';  ai') or (len(line.split()) > 4 and line.split()[4] == '4')]
        elif ptype == 'h':
            return self.contents[:self.get_bos('m')]
        elif ptype == 'f':
            term_line = self.contents.index(self.get_section('i')[-1]) + 1
            return self.contents[term_line:]

    def save_mod(self):
        path = self.fname.split('/')
        path[-1] = self.lesion.name + '-' + path[-1]
        outname = '/'.join(path)
        with open(outname, 'w') as outfile:
            for i in 'hmtbpadif':
                for line in self.dict[i]:
                    outfile.write(line)
                outfile.write('\n')

    def get_nums(self, res):  # one call per residue
        atoms = ("N7", "H7", "C8", "N1", "H1", "C2", "N3", "N9", "C4", "C5", "O5", "H3", "H41", "H42", "H71", "H72",
                 "C6", "O6", "H8", "N2", "O4'", "C1'", "C2'", "H1'", "O8", "H9", "H5", "O2", "O4", "N4", "H73", 'N6',
                 'PB', 'O4P', 'O5P', 'O5R', 'C5R', 'H5R1', 'H5R2', 'O4R', 'C1R', 'H1R', 'N1T', 'C6T', 'H6T', 'C5T',
                 'C5M', 'H5M1', 'H5M2', 'H5M3', 'C4T', 'O4T', 'N3T', 'H3T', 'C2T', 'O2T', 'C2R', 'H2R1', 'H2R2', 'C4R',
                 'H4R', 'C5A', 'H5A1', 'H5A2', 'H5A3', 'C3R', 'H3R', 'O3R', 'H64', 'H65', 'H66', 'H6', 'H61', 'H62')
        names = {}
        nums = {}
        for i in atoms:
            cont = [int(line.split()[0]) for line in self.dict['t'] if len(line.split()) > 7
                    and (line.split()[3] == self.lesion.initbase or line.split()[3] == self.lesion.name)
                    and line.split()[4] == i
                    and int(line.split()[2]) == res]
            if len(cont) > 0:
                names[cont[0]] = i
                nums[i] = cont[0]
        return names, nums
    
    def check_base(self, ret=False):
        for res in self.res:
            reslines = [x.split()[3] for x in self.dict['t'] if len(x.split()) > 7 and not x.strip().startswith(';')
                        and int(x.split()[2]) == res]
            if ret:
                assert len(set(reslines)) == 1
                return reslines[0]
            if not all([rn == self.lesion.initbase for rn in reslines]):
                raise RuntimeError("\n\n\n\t\tAt least some atoms in residue {} in file {} do not belong to residue {}."
                                   "\n\n\t\tPlease choose another residue or fix "
                                   "your residue naming.\n\n".format(res, self.fname, self.lesion.initbase))
    
    def choose_ap(self):
        base = self.check_base(ret=True)
        mods = {'DA': basemods.APA, 'DC': basemods.APC, 'DT': basemods.APT, 'DG': basemods.APG}
        return mods[base]
    
    def swap(self):
        for i in 'tbadi':
            section = self.dict[i]
            for lnum in range(len(section)):
                line = section[lnum]
                comm = line.find(';')
                if comm >= 0:
                    act_line = line[:comm]
                    comm = ' ' + line[comm:].rstrip()
                else:
                    act_line = line
                    comm = ''
                if len(act_line.split()) > (self.section_npar[i] + 1):
                    if i in 'badi':
                        section[lnum] = self.swap_param(act_line, i).rstrip() + comm + '\n'
                    elif i == 't':
                        section[lnum] = self.swap_atoms(act_line).rstrip() + comm + '\n'
            self.dict[i] = section
    
    @staticmethod
    def swap_atoms(act_line):
        fstring = "{:>6d}{:>11s}{:>7d}{:>7s}{:>7s}{:>7d}{:>11.4f}{:>11.3f}{:>7s}{:>11.4f}{:>11.3f}"
        lspl = act_line.split()
        lspl[1], lspl[8] = lspl[8], lspl[1]
        lspl[6], lspl[9] = lspl[9], lspl[6]
        lspl[7], lspl[10] = lspl[10], lspl[7]
        for i in [0, 2, 5]:
            lspl[i] = int(lspl[i])
        for i in [6, 7, 9, 10]:
            lspl[i] = float(lspl[i])
        return fstring.format(*lspl)
    
    def swap_param(self, act_line, ptype):
        param_values = act_line.split()[self.section_npar[ptype] + 1:]
        if not len(param_values) % 2 == 0:
            raise RuntimeError('Line {} contains an uneven number of parameters'.format(act_line))
        half = len(param_values) // 2
        new_param_values = param_values[half:] + param_values[:half]
        new_line = ' '.join('{:>5d}'.format(int(a)) for a in act_line.split()[:self.section_npar[ptype] + 1]) \
                   + "".join("{:>10}".format(a) for a in new_param_values)
        return new_line


class Pdb:
    def __init__(self, filename, res, ter, top):
        if isinstance(res, list):
            self.res = res  # list of residues to modify
        else:
            self.res = [res]  # one-element list
        self.fname = filename
        self.top = top
        self.contents = open(self.fname).readlines()
        self.lesion = None
        self.initbase = None
        self.ter = ter  # [resnr, resname, atomname] of original chain terminus

    def add_atoms(self):
        n_added = 0
        for res in self.res:
            atoms = self.lesion.get_new_atoms()
            for a in atoms.keys():
                hook = atoms[a][2]
                ref1 = atoms[a][0][0]
                ref2 = atoms[a][0][1]  # TODO cope with name_aliases?
                scale = atoms[a][1]
                h, b, e = self.find_line(hook, res), self.find_line(ref1, res), self.find_line(ref2, res)
                newline = self.gen_line(a, h, b, e, scale)
                ter = [n for n, x in enumerate(self.contents) if x.startswith("ATOM")
                       and int(x[22:26]) == int(res) and x[17:20].strip() == self.lesion.initbase][-1]
                self.contents.insert(ter + 1, newline)
                n_added += 1
                
    def remove_atoms(self):
        remove = [x for x in self.lesion.alttypes.keys() if self.lesion.alttypes[x][0].startswith("DUM")]
        for rem in remove:
            for res in self.res:
                line = self.find_line(rem, res)
                self.contents.pop(line)  # TODO renumber?
    
    def rename_atoms(self):
        to_mod = self.lesion.get_alttypes()
        for res in self.res:
            for atom in to_mod.keys():
                if len(to_mod[atom]) == 3:
                    line = self.find_line(atom, res)
                    self.contents[line] = self.contents[line][:12] + "{:>4s}".format(to_mod[atom][2]) + \
                                          self.contents[line][16:]
        

    def find_line(self, name, res):
        lines = [x for x in range(len(self.contents)) if self.contents[x].startswith("ATOM")
                 and int(self.contents[x][22:26]) == res and self.contents[x][12:16].strip() == name
                 and self.contents[x][17:20].strip().startswith('D')]
        if len(lines) > 1:
            raise RuntimeError("found more than one line fulfilling criterion:\n{}\n"
                               "Consider renumbering DNA residues in your PDB file to avoid duplicates"
                               .format(" ".join([self.contents[a] for a in lines])))
        elif len(lines) == 0:
            raise RuntimeError("name {} and resnum {} not found".format(name, res))
        return lines[0]
    
    def choose6ma(self):
        res = self.res[0]
        n7coord = self.extract_coords(self.find_line('N7', res))
        h61coord = self.extract_coords(self.find_line('H61', res))
        h62coord = self.extract_coords(self.find_line('H62', res))
        d1 = sum([(a - b) ** 2 for a, b in zip(n7coord, h61coord)]) ** 0.5
        d2 = sum([(a - b) ** 2 for a, b in zip(n7coord, h62coord)]) ** 0.5
        closer = "H61" if d1 < d2 else "H62"
        return closer

    def gen_line(self, name, hook, ref1, ref2, scale):
        hxyz = self.extract_coords(hook)
        hline = self.contents[hook]
        bxyz = self.extract_coords(ref1)
        exyz = self.extract_coords(ref2)
        nxyz = [hxyz[a] + scale*(exyz[a]-bxyz[a]) for a in range(3)]
        new = hline[:6] + '{:>5}'.format(999) + '{:>4}'.format(name) + hline[15:30] + \
            '{:>8.3f}{:>8.3f}{:>8.3f}'.format(*nxyz) + hline[54:]
        return new

    def extract_coords(self, l):
        line = self.contents[l]
        return [float(line[30+8*a:30+8*(a+1)]) for a in range(3)]

    def save_mod(self):
        path = self.fname.split('/')
        path[-1] = self.lesion.name + '-' + path[-1]
        outname = '/'.join(path)
        with open(outname, 'w') as outfile:
            for line in self.contents:
                outfile.write(line)
            outfile.write('\n')


def parse_options():
    parser = OptionParser(usage="%prog -p top_file -f struct_file -l lesion_name -r residues_to_mutate")
    parser.add_option("-p", dest="topfile", action="store", type="string",
                      help="topology file to be modified (preferably .itp)")
    parser.add_option("-f", dest="structfile", action="store", type="string",
                      help="structure file to be modified (.pdb)")
    parser.add_option("-l", "--lesion", dest="lesion", action="store", type="string",
                      help="lesion to be introduced: 8oxoG, FapyG or 5mC")
    parser.add_option("-r", "--res", dest="res", action="store", type="string",
                      help="residue number to be mutated (to pass multiple numbers, use quotation marks;"
                           "if the lesion spans two residues, pass the lower residue number, i.e. that on the 5' side)")
    parser.add_option("--noalch", dest="noalch", action="store_true",
                      help="produce a non-alchemical topology, i.e. single-state only")
    parser.add_option("-i", dest="invert", action="store_true",
                      help="swap states A and B, so that A corresponds to the modified and B the unmodified base")
    
    (opts, args) = parser.parse_args()
    opts.res = str(opts.res)
    opts.res = [int(x) for x in opts.res.split()]
    return opts

if __name__ == "__main__":
    opt = parse_options()
    if opt.lesion.lower() not in ['8oxog', 'fapyg', '5mc', 'ttd', '6ma', 'ap']:
        raise ValueError("Lesion has to be one of the following: 8oxoG, FapyG, 5mC, 6mA, TTd, AP")
    if opt.lesion.lower() != '6ma':
        t = Top(opt.topfile, opt.structfile, opt.lesion, opt.res, opt.noalch)
        t.process_top()
        if opt.lesion.lower() == 'ttd':
            t = Top(opt.topfile, opt.structfile, 'ttdd', [x+1 for x in opt.res], opt.noalch, base=t)
            t.process_top()
        if opt.invert:
            t.swap()
    else:  # in 6mA we have a variable methylation site, need to check for each instance
        t = Top(opt.topfile, opt.structfile, opt.lesion, [opt.res[0]], opt.noalch)
        t.process_top()
        for r in opt.res[1:]:
            t = Top(opt.topfile, opt.structfile, opt.lesion, [r], opt.noalch, base=t)
            t.process_top()
    t.save_mod()
    t.pdb.add_atoms()
    if opt.noalch:
        t.pdb.remove_atoms()
        t.pdb.rename_atoms()
    t.pdb.save_mod()
