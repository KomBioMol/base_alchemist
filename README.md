# Base Alchemist

`base_alchemist` is a Python script to introduce alchemical and non-alchemical DNA modifications to Gromacs topology and structure files.
It is also intended to serve as a curated and extensible repository of parametrized DNA lesions.

## Getting Started

The script is ready to use on any machine employed with a working Python distribution; there should be no need for installing extra libraries. Run "python alchemist.py -h" to see available options.

## Simple usage

  The script is currently capable of adding one or more lesions of the same type, choosing from available options:

+ 7,8-dihydro-8-oxoguanine (8oxoG)
+ 2,6-diamino-4-hydroxy-5-formamidopyrimidine (FapyG)
+ 5-methylcytosine (5mC)
+ N6-methyladenosine (6mA)
+ cyclic (cis-syn) thymine dimer (TTd)
+ abasic site (AP)

  Both topology (.itp/.top) and structure (.pdb) files are processed to insert state B parameters, dummies, additional and modified bonded terms. Output files are prefixed with the lesion name.

## Examples

  To run the script with the test files provided here, you might try:

```
python alchemist.py -f conf.pdb -p topol_DNA_chain_B.itp -l 5mc -r "22 23"
```

  General usage:

```
python alchemist.py -p top_file -f struct_file -l lesion_name -r residues_to_mutate [--noalch -i]
```

## Force field parameters

The `basemods.py` file contains all necessary parameter
modifications introduced to the standard Amber99sb/bsc1 force field.
The dictionaries inside the file contain formatted parameters, and can be 
modified according to the specification below:

+ `dummies` and `newatoms` define atoms that are added to the topology:
their type, charge and mass in states A and B (`dummies`), as well as
the geometrical placement rules for modification of the PDB files
(`newatoms`).
+ `alttypes` lists atoms that change types and/or masses when the lesion
is introduced.
+ `mod` and `new` are dictionaries that list bonded parameters that
should be either modified (`mod`) or created from scratch (`new`):
    + `b` defines bond parameters (two for state A, two for state B);
    + `p` defines pairs;
    + `a` defines angle parameters (two for state A, two for state B);
    + `d` defines proper dihedral parameters (three for state A, three
    for state B);
    + `i` defines improper dihedral parameters (three for state A, three
    for state B).